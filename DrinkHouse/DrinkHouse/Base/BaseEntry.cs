﻿using System.ComponentModel.DataAnnotations;

namespace DrinkHouse.Base
{
    public class BaseEntry
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public decimal Cost { get; set; }
    }
}
