﻿using System.Data.Entity;
using DrinkHouse.Models;

namespace DrinkHouse.DbContext
{
    internal class DrinkHouseContext : System.Data.Entity.DbContext
    {
        public DbSet<Drink> Drinks { get; set; }
        public DbSet<Additive> Additives { get; set; }
    }
}
