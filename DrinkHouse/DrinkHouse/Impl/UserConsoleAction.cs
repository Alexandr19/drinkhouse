﻿using System;
using System.Collections.Generic;
using System.Linq;
using DrinkHouse.Base;
using DrinkHouse.Interfaces;
using DrinkHouse.Models;

namespace DrinkHouse.Impl
{
    class UserConsoleAction : IUserConsoleAction
    {
        private const string ReturnToMainMenuMessage = "You returned to main menu\n";

        private readonly ICollectionManager<Drink> _drinkCollectionManager;
        private readonly ICollectionManager<Additive> _additivesCollectionManager;

        public UserConsoleAction(ICollectionManager<Drink> drinkCollectionManager, ICollectionManager<Additive> additivesCollectionManager)
        {
            if (null == drinkCollectionManager)
                throw new ArgumentNullException(nameof(drinkCollectionManager));

            if (null == additivesCollectionManager)
                throw new ArgumentNullException(nameof(additivesCollectionManager));

            _drinkCollectionManager = drinkCollectionManager;
            _additivesCollectionManager = additivesCollectionManager;
        }

        public void LoadDrinksCollection()
        {
            _drinkCollectionManager.LoadCostCollection();
        }

        public void LoadAdditivesCollection()
        {
            _additivesCollectionManager.LoadCostCollection();
        }

        public void ShowAllDrinks()
        {
            _drinkCollectionManager.ShowAll();
        }

        public void ShowAllAdditives()
        {
            _additivesCollectionManager.ShowAll();
        }

        public void AddDrink()
        {
            var baseEntry = GetInputBaseEntry("Drink");
            if (baseEntry == null)
            {
                Console.WriteLine(ReturnToMainMenuMessage);
                return;
            }

            _drinkCollectionManager.Add(
                new Drink
                {
                    Name = baseEntry.Name,
                    Cost = baseEntry.Cost
                });

            Console.WriteLine("You have successfully added new Drink and returned to main menu.\n");
        }

        public void AddAdditive()
        {
            var baseEntry = GetInputBaseEntry("Additive");
            if (baseEntry == null)
            {
                Console.WriteLine(ReturnToMainMenuMessage);
                return;
            }

            _additivesCollectionManager.Add(
                    new Additive
                    {
                        Name = baseEntry.Name,
                        Cost = baseEntry.Cost
                    });

            Console.WriteLine("You have successfully added new Additive and returned to main menu.\n");
        }

        public void DeleteDrink()
        {
            var id = GetEntryIdFromConsole("Drink");
            
            if (!_drinkCollectionManager.CostDictionary.ContainsKey(id) && id != 0)
            {
                id = RepeartInputEntryId(GetEntryIdFromConsole, "Drink", $"Id: '{id}' doesn't exists in Drinks Collection. Try again? y/n");
            }

            if (id == 0)
            {
                Console.WriteLine(ReturnToMainMenuMessage);
                return;
            }

            _drinkCollectionManager.Delete(id);
            Console.WriteLine($"You have successfully deleted Drink by Id: {id}.\n");
        }

        public void DeleteAdditive()
        {
            var id = GetEntryIdFromConsole("Additive");
            
            if (!_additivesCollectionManager.CostDictionary.ContainsKey(id) && id != 0)
            {
                id = RepeartInputEntryId(GetEntryIdFromConsole, "Additive", $"Id: '{id}' doesn't exists in Additives Collection. Try again? y/n");
            }

            if (id == 0)
            {
                Console.WriteLine(ReturnToMainMenuMessage);
                return;
            }

            _additivesCollectionManager.Delete(id);
            Console.WriteLine($"You have successfully deleted Additive by Id: {id}.\n");
        }

        public void BuyDrink()
        {
            if (!_drinkCollectionManager.CacheEntryCollection.Any())
            {
                Console.WriteLine($"Drinks collection is empty. {ReturnToMainMenuMessage}");
                return;
            }

            if (!_additivesCollectionManager.CacheEntryCollection.Any())
            {
                Console.WriteLine($"Additives collection is empty. {ReturnToMainMenuMessage}");
                return;
            }

            var drinkId = GetEntryIdFromConsole("Drink");
            
            if (!_drinkCollectionManager.CostDictionary.ContainsKey(drinkId) && drinkId != 0)
            {
                drinkId = RepeartInputEntryId(GetEntryIdFromConsole, "Drink", $"Id: '{drinkId}' doesn't exists in Drinks Collection. Try again? y/n");
            }

            if (drinkId != 0)
            {
                var additiveIds = GetAdditiveIdsFromConsole()?.ToList();

                if (additiveIds != null && additiveIds.Any())
                {
                    decimal additivesCost = additiveIds.Sum(additiveId => _additivesCollectionManager.CostDictionary[additiveId]);
                    decimal drinkCost = _drinkCollectionManager.CostDictionary[drinkId];

                    Console.WriteLine($"Cost: {drinkCost + additivesCost} $");
                }
                else
                {
                    Console.WriteLine(ReturnToMainMenuMessage);
                    return;
                }
            }

            Console.WriteLine(ReturnToMainMenuMessage);
        }

        private BaseEntry GetInputBaseEntry(string entryName)
        {
            Console.WriteLine($"Input {entryName} info:");

            var name = GetEntryNameFromConsole();
            var cost = GetEntryCostFromConsole();

            if (cost == 0 || string.IsNullOrEmpty(name))
            {
                return null;
            }

            var entry = new BaseEntry
            {
                Name = name,
                Cost = cost
            };

            return entry;
        }

        private int GetEntryIdFromConsole(string typeName)
        {
            Console.Write($"{typeName} Id: ");
            string idString = Console.ReadLine();

            int id;
            var isValidIdValue = int.TryParse(idString, out id);
            if (!isValidIdValue || id == 0)
            {
                id = RepeartInputEntryId(GetEntryIdFromConsole, typeName, "Id value is incorrect. Try again? y/n");
            }

            return id;
        }

        private decimal GetEntryCostFromConsole()
        {
            Console.Write("Cost: ");
            var costString = Console.ReadLine();

            decimal cost;
            var isValidCostValue = decimal.TryParse(costString, out cost);
            if (!isValidCostValue || cost == 0)
            {
                Console.WriteLine("Cost value is incorrect. Try again? y/n");
                var command = Console.ReadLine()?.ToLower();
                if (command == "y")
                {
                    cost = GetEntryCostFromConsole();
                }
            }

            return cost;
        }

        private string GetEntryNameFromConsole()
        {
            Console.Write("Name: ");
            var nameString = Console.ReadLine()?.Trim(' ');
            
            if (string.IsNullOrEmpty(nameString))
            {
                Console.WriteLine("Name is required. Try again? y/n");
                var command = Console.ReadLine()?.ToLower();
                if (command == "y")
                {
                    nameString = GetEntryNameFromConsole();
                }
            }

            return nameString;
        }

        private IList<int> GetAdditiveIdsFromConsole()
        {
            Console.Write("Additive Ids (by comma separator):");

            var idStrings = Console.ReadLine()?.TrimEnd(',', '.').Split(',').ToArray();

            var outputList = new List<int>();

            if (idStrings != null)
            {
                foreach (var idString in idStrings)
                {
                    int id;
                    var isValidIdValue = int.TryParse(idString, out id);

                    if (!isValidIdValue || id == 0)
                    {
                        Console.WriteLine($"Id value - '{idString}' is incorrect");
                        continue;
                    }

                    if (!_additivesCollectionManager.CostDictionary.ContainsKey(id))
                    {
                        Console.WriteLine($"Id value - '{idString}' doesn't exists in Additives Collection");
                        continue;
                    }

                    outputList.Add(id);
                }
                if (idStrings.Length != outputList.Count)
                {
                    outputList = RepeartInputAdditiveIds(GetAdditiveIdsFromConsole, "Input collection is incorrect. Try again? y/n").ToList();
                }
            }
            else
            {
                outputList = RepeartInputAdditiveIds(GetAdditiveIdsFromConsole, "Input collection is empty. Try again? y/n").ToList();
            }

            return outputList;
        }

        private int RepeartInputEntryId(Func<string, int> method, string typeName, string message)
        {
            Console.WriteLine(message);
            var command = Console.ReadLine()?.ToLower();
            if (command == "y")
            {
                return method(typeName);
            }

            return 0;
        }

        private IList<int> RepeartInputAdditiveIds(Func<IList<int>> method, string message)
        {
            Console.WriteLine(message);
            var command = Console.ReadLine()?.ToLower();
            if (command == "y")
            {
                return method();
            }

            return new List<int>();
        }
        
    }
}
