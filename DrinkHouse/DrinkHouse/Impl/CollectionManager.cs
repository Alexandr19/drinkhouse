﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrinkHouse.Base;
using DrinkHouse.Interfaces;

namespace DrinkHouse.Impl
{
    public class CollectionManager<T> : ICollectionManager<T> where T : BaseEntry, new()
    {
        const int TableWidth = 67;
        const int IdColumnWidth = 5;
        const int NameColumnWidth = 50;
        const int CostColumnWidth = 8;

        private readonly IStorageManager<T> _databaseManager;

        public IDictionary<int, decimal> CostDictionary { get; set; }

        public IList<T> CacheEntryCollection { get; set; }

        public CollectionManager(IStorageManager<T> databaseManager)
        {
            if (null == databaseManager)
                throw new ArgumentNullException(nameof(databaseManager));

            _databaseManager = databaseManager;
            CostDictionary = new Dictionary<int, decimal>();
            CacheEntryCollection = new List<T>();
        }

        public void Add(T entry)
        {
            _databaseManager.Add(entry);
            CostDictionary.Add(entry.Id, entry.Cost);
            CacheEntryCollection.Add(entry);
        }

        public void Delete(int id)
        {
            _databaseManager.Delete(x => x.Id == id);
            CostDictionary.Remove(id);
            CacheEntryCollection.Remove(CacheEntryCollection.FirstOrDefault(x => x.Id == id));
        }

        public void LoadCostCollection()
        {
            if (!_databaseManager.CheckAny())
            {
                Console.WriteLine($"{typeof(T).Name} collection is empty in database.");
                return;
            }

            var entryCollection = _databaseManager.GetAll();
            foreach (var drink in entryCollection)
            {
                CostDictionary.Add(drink.Id, drink.Cost);
                CacheEntryCollection.Add(drink);
            }
        }

        public void ShowAll()
        {
            if (!CacheEntryCollection.Any())
            {
                Console.WriteLine($"{typeof(T).Name}s collection is empty");
                return;
            }

            var drinksInfoBuilder = new StringBuilder();
            drinksInfoBuilder.Append(new string('-', TableWidth));
            drinksInfoBuilder.Append($"\n{typeof(T).Name} collection:\n");
            drinksInfoBuilder.Append(new string('-', TableWidth));
            drinksInfoBuilder.Append($"\n|{"Id".PadRight(IdColumnWidth)}|{"Name".PadRight(NameColumnWidth)}|{"Cost".PadRight(CostColumnWidth)}|\n");
            drinksInfoBuilder.Append(new string('-', TableWidth));

            foreach (var drink in CacheEntryCollection)
            {
                drinksInfoBuilder.Append(
                    $"\n|{drink.Id.ToString().PadRight(IdColumnWidth)}|{drink.Name.PadRight(NameColumnWidth)}|{drink.Cost.ToString("C").PadRight(CostColumnWidth)}|\n");

                drinksInfoBuilder.Append(new string('-', TableWidth));
            }

            Console.WriteLine(drinksInfoBuilder + "\n");
        }
    }
}
