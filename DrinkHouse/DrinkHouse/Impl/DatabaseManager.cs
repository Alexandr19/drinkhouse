﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DrinkHouse.DbContext;
using DrinkHouse.Interfaces;

namespace DrinkHouse.Impl
{
    class DatabaseManager<T> : IStorageManager<T> where T : class, new()
    {
        public bool CheckAny()
        {
            using (var dbContext = new DrinkHouseContext())
            {
                return dbContext.Set<T>().Any();
            }
        }
        
        public IList<T> GetAll()
        {
            using (var dbContext = new DrinkHouseContext())
            {
                return dbContext.Set<T>().ToList();
            }
        }
        
        public void Add(T entry)
        {
            using (var dbContext = new DrinkHouseContext())
            {
                dbContext.Set<T>().Add(entry);
                dbContext.SaveChanges();
            }
        }
        
        public void Delete(Expression<Func<T, bool>> predicate)
        {
            using (var dbContext = new DrinkHouseContext())
            {
                var entry = dbContext.Set<T>().FirstOrDefault(predicate);
                if (entry != null)
                {
                    dbContext.Set<T>().Remove(entry);
                    dbContext.SaveChanges();
                }
            }
        }
    }
}
