﻿using System;
using System.Collections.Generic;
using System.Text;
using DrinkHouse.Impl;
using DrinkHouse.Interfaces;
using DrinkHouse.Models;

namespace DrinkHouse
{
    class Program
    {
        private static readonly IUserConsoleAction UserConsoleAction;
        private static readonly Dictionary<string, Action> ActionDictionary;

        static Program()
        {
            IStorageManager<Drink> drinkDatabaseManager = new DatabaseManager<Drink>();
            var drinkCollectionManager = new CollectionManager<Drink>(drinkDatabaseManager);

            IStorageManager<Additive> additiveDatabaseManager = new DatabaseManager<Additive>();
            var additivesCollectionManager = new CollectionManager<Additive>(additiveDatabaseManager);

            UserConsoleAction = new UserConsoleAction(drinkCollectionManager, additivesCollectionManager);
            ActionDictionary = new Dictionary<string, Action>
            {
                { "show_drinks", UserConsoleAction.ShowAllDrinks },
                { "show_additives", UserConsoleAction.ShowAllAdditives},
                { "add_drink", UserConsoleAction.AddDrink},
                { "add_additive", UserConsoleAction.AddAdditive},
                { "delete_drink", UserConsoleAction.DeleteDrink},
                { "delete_additive", UserConsoleAction.DeleteAdditive},
                { "buy_drink", UserConsoleAction.BuyDrink},
                { "exit", () => { Environment.Exit(0); } }
            };
        }


        static void Main(string[] args)
        {
            try
            {
                WriteUserManual();

                Console.WriteLine("Loading Drinks and Additives collections. Please wait...");
                UserConsoleAction.LoadDrinksCollection();
                UserConsoleAction.LoadAdditivesCollection();
                Console.WriteLine("Loading is completed\n");

                Console.WriteLine("Enter command:\n");
                while (true)
                {
                    var command = Console.ReadLine() ?? string.Empty;

                    if (ActionDictionary.ContainsKey(command))
                    {
                        ActionDictionary[command].Invoke();
                    }
                    else
                    {
                        Console.WriteLine("Command not recognized. Try again");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
        

        static void WriteUserManual()
        {
            var manualStringBuilder = new StringBuilder();
            manualStringBuilder.Append("***** User Manual *****\n\n");

            manualStringBuilder.Append("Commands:\n");
            manualStringBuilder.Append("Show all Drinks - show_drinks\n");
            manualStringBuilder.Append("Swow all Additives - show_additives\n");
            manualStringBuilder.Append("Buy Drink - buy_drink\n");
            manualStringBuilder.Append("Add Drink - add_drink\n");
            manualStringBuilder.Append("Add Additive - add_additive\n");
            manualStringBuilder.Append("Delete Drink - delete_drink\n");
            manualStringBuilder.Append("Delete Additive - delete_additive\n");
            manualStringBuilder.Append("For close program - exit\n");

            Console.WriteLine(manualStringBuilder.ToString());
        }

    }
}
