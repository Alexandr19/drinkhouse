﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DrinkHouse.Interfaces
{
    public interface IStorageManager<T> where T : class, new()
    {
        bool CheckAny();

        IList<T> GetAll();

        void Add(T entry);

        void Delete(Expression<Func<T, bool>> predicate);
    }
}
