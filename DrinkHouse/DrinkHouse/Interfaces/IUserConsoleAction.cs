﻿
namespace DrinkHouse.Interfaces
{
    interface IUserConsoleAction
    {
        void LoadDrinksCollection();

        void LoadAdditivesCollection();

        void ShowAllDrinks();

        void ShowAllAdditives();

        void AddDrink();

        void AddAdditive();

        void DeleteDrink();

        void DeleteAdditive();

        void BuyDrink();
    }
}
