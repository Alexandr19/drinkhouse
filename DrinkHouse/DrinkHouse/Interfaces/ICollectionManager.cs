﻿using System.Collections.Generic;

namespace DrinkHouse.Interfaces
{
    interface ICollectionManager<T> where T : new()
    {
        IDictionary<int, decimal> CostDictionary { get; set; }

        IList<T> CacheEntryCollection { get; set; } 

        void LoadCostCollection();

        void Add(T entity);

        void Delete(int id);

        void ShowAll();
    }
}
